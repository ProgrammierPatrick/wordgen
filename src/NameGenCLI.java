import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

/** Command Line Interface for NameGen system */
public class NameGenCLI {
	private static final String allGeneratorsText = "Matrix";

	public static class CLIOption {
		private String longcmd;
		private String shortcmd;
		private String argtext;
		private String text;
		
		public CLIOption(String longcmd, String shortcmd, String argtext, String text) {
			this.longcmd = longcmd;
			this.shortcmd = shortcmd;
			this.argtext = argtext;
			this.text = text;
		}
		
		public boolean check(String arg) {
			return arg.equalsIgnoreCase("--" + longcmd) || arg.equalsIgnoreCase("-" + shortcmd);
		}
		public String getHelp() {
			return "  -" + shortcmd + ", --" + longcmd + " " + argtext + "\n    " + text + "\n";
		}
	}
	
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		CLIOption opgeneratefrom = new CLIOption("generate-from", "gf", "[filename]", "Generate a new Generator using the text in the file. Specify the generator using --generator.");
		CLIOption opgeneratefromstd = new CLIOption("generate-from-stdin", "gi", "", "Like --generate-from, but uses the standard input as input");
		CLIOption opwriteto = new CLIOption("write-to", "w", "[filename]", "Writes the Generator config to the file.");
		CLIOption opwritetostd = new CLIOption("write-to-stdout", "wo", "", "Like --write-to, but writes to the standard output.");
		CLIOption opreadfrom = new CLIOption("read-from", "r", "[filename]", "Reads the Generator config from the file. Make sure to specify the correct generator with --generator.");
		CLIOption opreadfromstd = new CLIOption("read-from-stdin", "ri", "", "Line --read-from, but reads from standard input");
		CLIOption opgenerator = new CLIOption("generator", "g", "[generator]", "Chooses the used generator. Options are: " + allGeneratorsText);
		CLIOption opgeneratewords = new CLIOption("generate-words", "n", "[count]", "Chooses the number of words that get generated.");
		CLIOption opgenerateword = new CLIOption("generate-word", "1", "", "Like --generate-words, but generate only one word");
		CLIOption ophelp = new CLIOption("help", "h", "", "Show this help");
		
		String helpText = "java -jar NameGen.jar [OPTIONS]\n"
				+ "where OPTIONS are:\n"
				+ opgeneratefrom.getHelp()
				+ opgeneratefromstd.getHelp()
				+ opwriteto.getHelp()
				+ opwritetostd.getHelp()
				+ opreadfrom.getHelp()
				+ opreadfromstd.getHelp()
				+ opgenerator.getHelp()
				+ opgeneratewords.getHelp()
				+ opgenerateword.getHelp()
				+ ophelp.getHelp();
				
		
		String generateFrom = null;
		String readFrom = null;
		String saveTo = null;
		String generator = null;
		int generateWords = 10;
		
		if(args.length == 0) {
			System.out.println("No arguments specified.");
			System.out.println(helpText);
			return;
		}
		for(int i=0;i<args.length;++i) {
			if(opgeneratefrom.check(args[i])) {
				generateFrom = args[i+1];
				i++;
			} else if(opgeneratefromstd.check(args[i])) {
				generateFrom = "STDIN";
			} else if(opwriteto.check(args[i])) {
				saveTo = args[i+1];
				i++;
			} else if(opwritetostd.check(args[i])) {
				saveTo = "STDOUT";
			} else if(opreadfrom.check(args[i])) {
				readFrom = args[i+1];
				i++;
			} else if(opreadfromstd.check(args[i])) {
				readFrom = "STDIN";
			} else if(opgenerator.check(args[i])) {
				generator = args[i+1];
				i++;
			} else if(opgenerateword.check(args[i])) {
				generateWords = 1;
			} else if(opgeneratewords.check(args[i])) {
				generateWords = Integer.parseInt(args[i+1]);
				i++;
			} else if(args[i].equals("--help")) {
				System.out.println(helpText);
				return;
			} else {
				System.out.println("Syntax error in argument '" + args[i] + "'");
				System.out.println(helpText);
				return;
			}
		}
		
		Generator gen = null;
		if(generator == null) gen = new TransitionMatrixGenerator();
		else if(generator.equals("Matrix")) gen = new TransitionMatrixGenerator();
		else {
			System.out.println("Generator '" + generator + "' is not known. Possible genrators: " + allGeneratorsText);
			return;
		}
		
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
		
		if(generateFrom != null) {
			if(generateFrom.equals("STDIN")) gen.generateFromText(stdin);
			else gen.generateFromText(new FileReader(generateFrom));
		}
		
		if(readFrom != null) {
			BufferedReader r = null;
			if(readFrom.equals("STDIN")) r = stdin;
			else r = new BufferedReader(new FileReader(readFrom));
			gen.loadFromText(r.lines().collect(Collectors.joining("\n")));
		}
		
		if(saveTo != null) {
			String data = gen.saveToText();
			if(saveTo.equals("STDOUT")) System.out.println(data);
			else new FileWriter(saveTo).write(data);
		}
		
		for(int i = 0; i < generateWords; ++i) {
			System.out.print(gen.generateWord() + " ");
		}
	}

}
