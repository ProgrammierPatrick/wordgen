import java.io.IOException;
import java.io.Reader;
import java.util.Random;

public class TransitionMatrixGenerator implements Generator {
	/* prbability matrix: value BORDER means start or end of word */
	private float[][] matrix;
	private static final int SIZE = 27;
	private static final int BORDER = 26;
	
	private Random random;
	
	public TransitionMatrixGenerator() {
		matrix = new float[SIZE][SIZE];
		random = new Random();
	}
	
	@Override
	public void generateFromText(Reader reader) throws IOException {
		// clear matrix to zero
		for(int i = 0; i < SIZE; ++i) {
			for( int j = 0; j < SIZE; ++j) {
				matrix[i][j] = 0.0f;
			}
		}
		
		// interpret input text
		int lastVal = BORDER;
		int numWords = 0;
		while(true) {
			int currVal = 0;
			boolean eof = false; 
			
			int c = reader.read();
			if(c >= 'a' && c <= 'z') {
				currVal = c - 'a';
			} else if(c >= 'A' && c <= 'Z') {
				currVal = c - 'A';
			} else if(c == -1) {
				eof = true;
				currVal = BORDER;
			} else {
				currVal = BORDER; // word border
			}
			
			if(lastVal == BORDER && currVal == BORDER) {
				if(eof) break;
				else continue; // we do not want to count SPACE SPACE as transision
			}
			
			matrix[lastVal][currVal]++;
			lastVal = currVal;
			
			numWords++;
			if(numWords % 1000 == 0) System.out.println("processed "+numWords + " words");
			
			if(eof) break;
		}
		System.out.println("processed " + numWords + " words");
		
		// normalize matrix rows
		for(int last = 0; last < SIZE; ++last) {
			
			// compute sum of row 
			float total = 0.0f;
			for( int curr = 0; curr < SIZE; ++curr) {
				total += matrix[last][curr];
			}
			
			if(total == 0.0f) {
				// Assume equal distribution if no data is available
				for(int curr = 0; curr < SIZE; ++curr) {
					matrix[last][curr] = 1.0f / 27.0f;
				}
			}
			else {
				for( int curr = 0; curr < SIZE; ++curr) {
					matrix[last][curr] /= total;
				}
			}
		}
		
		System.out.println("matrix normalized");
	}

	@Override
	public String saveToText() {
		StringBuilder text = new StringBuilder();
		for(int row = 0; row < SIZE; ++row) {
			for(int col = 0; col < SIZE; ++col) {
				text.append(matrix[row][col] + ",");
			}
			text.append("\n");
		}
		
		return text.toString();
	}

	@Override
	public String generateWord() {
		int lastVal = BORDER;
		StringBuilder sb = new StringBuilder();
		
		while(true) {
			if(sb.length() > 100) return sb.toString();
			
			float rand = random.nextFloat();
			
			float distribution = 0.0f;
			for(int i=0; i<SIZE; ++i) {
				distribution += matrix[lastVal][i];
				
				if(rand <= distribution) {
					// use char i
					lastVal = i;
					
					if(i == BORDER) return sb.toString();
					else sb.append((char)('a' + i));
					break;
				}
			}
			
		}
	}

	@Override
	public void loadFromText(String text) {
		throw new RuntimeException("Not implemented");
	}


}
