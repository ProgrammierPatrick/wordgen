import java.io.IOException;
import java.io.Reader;

public interface Generator {
	public void generateFromText(Reader reader) throws IOException;
	
	public void loadFromText(String text);
	public String saveToText();
	
	public String generateWord();
}
